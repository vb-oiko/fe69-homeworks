// 1) написать функцию pluck, которая берет массив объектов и возвращает массив значений определенного поля:
// var characters = [
// { 'name': 'barney', 'age': 36 },
// { 'name': 'fred', 'age': 40 }
// ];
// console.log(pluck(characters, 'name')); // ['barney', 'fred']

(function(){
    var characters = [
    { 'name': 'barney', 'age': 36 },
    { 'name': 'fred', 'age': 40 }
    ];

    var pluck = function (chars, field){
        var arr = [];
        for (let i=0;i<chars.length;i++){
            arr.push(chars[i][field]);
        }
        return arr;
    };

    console.log(pluck(characters, 'name'));
    console.log(pluck(characters, 'age'));
    


})();
