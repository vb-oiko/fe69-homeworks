// 2) Там где мы создавали круги в квадрате, дописать следующую логику: выбирать не только квадрат и круг, 
// а и прямоугольник и овал. Соответственно, если выбран или прямоугольник или овал - выводить 2 инпута 
// для ввода высоты и ширины.
// 3) Сделать проверки: чтобы нельзя было кликать сверху на другой круг и чтобы нельзя было создавать 
// фигуру с шириной/высотой больше самой area.
// 4) Если создаете фигуру с белым фоном - добавлять к ней рамку чтобы фигуру было видно

(function () {

    var defaultColor = '#000000';
    var defaultWidth = 25;
    var defaultHeight = 25;
    
    area.addEventListener('click', function (e) {
        
        var target = e.target;
        // 3) Сделать проверку чтобы нельзя было кликать сверху на другой круг 
        if (target != area) return;
        
        let elColor = defaultColor;
        let elWidth = defaultWidth;
        let elHeight = defaultHeight;
        let elType;

        var figureInput = document.querySelectorAll('[name="figure"]');
        for (var i = 0; i < figureInput.length; i++) {
            if (figureInput[i].checked) {
                elType = figureInput[i].value;
                
                let propList = figureInput[i].classList; //getting the list of the properties of the figure
                let propInput;
                for (let i=0;i<propList.length;i++){
                    propInput = document.getElementById(propList[i]); //trying to get an input with a property
                    if (propInput) { //if such property element exists
                        switch (propInput.id) {
                            case 'figColor': 
                                elColor = propInput.value;
                                break;
                            case 'figWidth': 
                                elWidth = propInput.value;
                                break;
                            case 'figHeight': 
                                elHeight = propInput.value;
                                break;
                            
                        }
                        
                        
                    }
                    
                } 
            }
        }

        // Сделать проверку чтобы нельзя было создавать фигуру с шириной/высотой больше самой area.
        if (elWidth>=area.offsetWidth || elHeight>=area.offsetHeight) {
            alert('Width and height cannot exceed canvas size: '+ 
                area.offsetWidth + 'px * '+ area.offsetHeight + 'px');
            return;
        }
        
        var el = document.createElement('div');
        el.classList.add('figure');
        el.classList.add(elType);
        el.style.top = e.offsetY + 'px';
        el.style.left = e.offsetX + 'px';
        el.style.width = elWidth + 'px';
        el.style.height = elHeight + 'px';    
        el.style.backgroundColor = defaultColor;
        el.style.backgroundColor = elColor;
        
        // 4) Если создаете фигуру с белым фоном - добавлять к ней рамку чтобы фигуру было видно
        if (el.style.backgroundColor === 'rgb(255, 255, 255)') el.style.borderColor = '#000000';
        target.appendChild(el);


    });


    // 2) Там где мы создавали круги в квадрате, дописать следующую логику: выбирать не только квадрат и круг, 
    // а и прямоугольник и овал. Соответственно, если выбран или прямоугольник или овал - выводить 2 инпута 
    // для ввода высоты и ширины.

    var figTypeInputs = document.querySelectorAll('.figType');
    var figPropertyInputs = document.querySelectorAll('.figProperty');

    for (let i=0;i<figTypeInputs.length;i++){
        //getting all input elements for figure type
        figTypeInputs[i].addEventListener('click', function(){

            //hiding all inputs for figure properties
            for (let i=0;i<figPropertyInputs.length;i++){
                figPropertyInputs[i].classList.add('hide');
                figPropertyInputs[i].parentElement.classList.add('hide');
            }

            let propList = this.classList; //getting the list of the properties of the figure
            let propInput;
            for (let i=0;i<propList.length;i++){
                propInput = document.getElementById(propList[i]); //trying to get an input with a property
                if (propInput) { //if such proerty element exists
                    propInput.classList.remove('hide');
                    propInput.parentElement.classList.remove('hide');
                    
                }
            }    
            
        });
    }

    figWidth.value = 25;
    figHeight.value = 25;
    document.querySelector('input[value="circle"]').click();


}());