var color;
var image = '';
var memory;
var price = 0;

cancelled = false;
while (true) {
    color = prompt('Color (red/white/silver)?');
    if (color === null) {
        cancelled = true;
        break;
    }
    if (color === 'red') {
        price += 5000;
        image = 'red-iphone.png';
        break;
    } else if (color === 'black') {
        price += 6000;
        image = 'black1.png';
        break;
    } else if (color === 'silver') {
        price += 7000;
        image = 'silver.png';
        break;
    }
    alert('Wrong color! Please try again (red/white/silver)');
}

if (!cancelled){
    while (true) {
        memory = prompt('Memory (128/256/512)?');
        if (memory === null) {
            cancelled = true;
            break;
        }
        if (+memory === 128) {
            price += 500;
            break;
        } else if (+memory === 256) {
            price += 700;
            break;
        } else if (+memory === 512) {
            price += 900;
            break;
        }
        alert('Wrong memory! Please try again (128/256/512)');
    }
}

if (cancelled){
    document.write('<h1>Thank you! Bye!</h1>');
} else {
    document.write ('<img src="../img/' + image + '" alt="">');
    document.write ('<h1>Price ' + price + '$</h1>');
};