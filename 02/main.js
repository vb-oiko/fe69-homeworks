// Есть какие-то товары. У каждого из них есть название и вес (weight). И для того, чтобы товары отправить, 
// мы должны их упаковать в коробки. Коробки есть стандартных размеров: small, medium, large. 
// В эти коробки мы пакуем товары по весу. В коробку small мы можем паковать товар до 3-х кг. 
// В коробку medium товары до 7 кг. В коробки large товары до 20кг.. Задача такая. Спрашивать у пользователя, 
// какие товары он хочет купить (у товара есть название, как писалось выше). Спрашивать до тех пор, 
// пока он не нажмет отмена. И в итоге выдавать количество и размеры коробок. К примеру, я выбираю 3 товара 
// общим весом: 41кг. Нам понадобятся коробки large (2 штуки) и одна medium.
// Товары придумывает сами. Коробки я дал. Итоговый вывод будет примерно таким: x2 - medium. x1 - small. 
// ПРИМЕРНО. Можете придумать интереснее.

// как вы поняли, ДА, ТОВАР МОЖНО ДРОБИТЬ
// представьте что это песок...


let items = [
    {
        name : 'wheat',
        weight : 19
    },
    {
        name : 'flour',
        weight : 5
    },
    {
        name : 'sugar',
        weight : 3
    },
    {
        name : 'coffee',
        weight : 1
    },
    {
        name : 'rice',
        weight : 15
    }
];

let boxes = [
    {
        size : 'large',
        capacity : 20,
        count : 0
    },
    {
        size : 'medium',
        capacity : 7,
        count : 0
    },
    {
        size : 'small',
        capacity : 3,
        count : 0
    }
];

let totalWeight = 0;

while(true) {
    itemName = prompt('Please enter the name of the item you want to add to the cart:');
    if (itemName === null) break;
    let found = false;
    for(let i=0; i<items.length;i++){
        if(items[i].name===itemName) {
            totalWeight += items[i].weight;
            found = true;
            break;
        } 
    }
    if(!found) alert("Sorry, We don't have " + itemName + ' in stock at the moment');
}

if (totalWeight === 0) {
    alert('Good bye! Hope you fill find something for you next time.');
} else {
    document.write('<p>Total weight: ' + totalWeight + '</p><ul>');
    
    for (let i=0;i<boxes.length;i++){
        boxes[i].count = Math.floor(totalWeight / boxes[i].capacity);
        totalWeight -= boxes[i].capacity * boxes[i].count;
    }
    if (totalWeight > 0) boxes[boxes.length - 1].count ++;

    for(let i=0;i<boxes.length;i++)
        if (boxes[i].count !== 0) 
            document.write('<li>x' + boxes[i].count + ' - ' + boxes[i].size + ' (' + boxes[i].capacity + ')</li>');
    
    document.write('</ul>');
}