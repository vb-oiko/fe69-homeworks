import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkplaceComponent } from './components/workplace/workplace.component';
import { InputComponent } from './components/input/input.component';
import { StatComponent } from './components/stat/stat.component';
import { FormsModule } from "@angular/forms";
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  imports: [
    CommonModule, FormsModule, MatSelectModule, 
    MatFormFieldModule, MatInputModule, MatButtonModule,
    MatCardModule, MatGridListModule,
  ],
  declarations: [WorkplaceComponent, InputComponent, StatComponent],
  exports: [WorkplaceComponent, InputComponent, StatComponent],
})
export class MoneyManagerModule { }
