// 3) Доделать компонент input. (ngModel - директива). 
// Форма с вводом sum, descr, селект со всеми категориями 
// и тип операции такой же, как и у выбранной категории.

// Декоратор Output (new EventEmitter)
// https://material.angular.io/

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Operation } from '../workplace/workplace.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {

  operation: Operation = {
    'id': '',
    'catId': '',
    'type': '',
    'sum': 100,
    'descr': ''
  }

  setCategory(id) { 
    this.operation.catId = id;
    this.operation.type = this.categories.find(elem => elem['id'] == id).type
  };

  btnCkick() {

    this.operation.id = Math.random().toString(36).substring(2, 15) + 
      Math.random().toString(36).substring(2, 15);
    
    this.newOperation.emit(this.operation);
  };

  @Input() categories;

  @Output() newOperation = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
