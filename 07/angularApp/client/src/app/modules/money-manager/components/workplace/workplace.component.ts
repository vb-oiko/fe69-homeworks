import { Component, OnInit } from '@angular/core';
// import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-workplace',
  templateUrl: './workplace.component.html',
  styleUrls: ['./workplace.component.scss']
})

export class WorkplaceComponent implements OnInit {
  
  operations = [
    {
      'id': '5bd8c46fc35a22b35e455b6f',
      'catId': '5bd8c46fd03c03ebf2b728a5',
      'type': 'consumption',
      'sum': 8633,
      'descr': 'commodo dolore dolor'
    },
    {
      'id': '5bd8c46f52ca026a14c20b0a',
      'catId': '5bd8c46fa6905822d10ce177',
      'type': 'consumption',
      'sum': 1920,
      'descr': 'quis aliquip consequat'
    },
    {
      'id': '5bd8c46f8817dd9cab26b589',
      'catId': '5bd8c46f36fbc0b8e5436ced',
      'type': 'income',
      'sum': 2746,
      'descr': 'non aute fugiat'
    },
    {
      'id': '5bd8c46f44600bfd63a67c5d',
      'catId': '5bd8c46fd03c03ebf2b728a5',
      'type': 'income',
      'sum': 1247,
      'descr': 'sunt sit quis'
    },
    {
      'id': '5bd8c46f489b6bea739f8d86',
      'catId': '5bd8c46fa6905822d10ce177',
      'type': 'consumption',
      'sum': 2689,
      'descr': 'cillum sunt culpa'
    },
    {
      'id': '5bd8c46f0d5b8dfd6708422a',
      'catId': '5bd8c46f402ca41286931379',
      'type': 'income',
      'sum': 333,
      'descr': 'nisi voluptate Lorem'
    },
    {
      'id': '5bd8c46f0dabeeb6b26aabb5',
      'catId': '5bd8c46f402ca41286931379',
      'type': 'consumption',
      'sum': 1482,
      'descr': 'est deserunt reprehenderit'
    },
    {
      'id': '5bd8c46f27b1129e85e40cbf',
      'catId': '5bd8c46f36fbc0b8e5436ced',
      'type': 'income',
      'sum': 2546,
      'descr': 'cillum et ad'
    },
    {
      'id': '5bd8c46f66fefbcab4558e1e',
      'catId': '5bd8c46fc1b416ae341c922f',
      'type': 'consumption',
      'sum': 366,
      'descr': 'eiusmod sit quis'
    },
    {
      'id': '5bd8c46f6701e97e3271ae81',
      'catId': '5bd8c46f1257fe7d1c2a3adb',
      'type': 'income',
      'sum': 802,
      'descr': 'officia excepteur in'
    },
    {
      'catId': '5bd8c46f36fbc0b8e5436ced',
      'descr': 'werwer',
      'sum': 234234,
      'type': 'consumption',
      'id': 'VShMLBU'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'f9UfHzo'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'd6n_6qt'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'qLfbyRn'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'gmjvXPa'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'cUYRBj_'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'ITZFBuV'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'YsgSs2C'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'ukEaKa0'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'rpeegKF'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'eL0d4P3'
    },
    {
      'catId': '5bd8c46fc5774aa7d174202c',
      'descr': 'werwer',
      'sum': 324,
      'type': 'income',
      'id': 'csxu08C'
    },
    {
      'catId': '5bd8c46f0c93161cbcace56f',
      'descr': 'werewr',
      'sum': 23423,
      'type': 'consumption',
      'id': 'Z7NuY9H'
    },
    {
      'catId': '5bd8c46f561eca76ba7d1468',
      'descr': 'werwer',
      'sum': 234,
      'type': 'consumption',
      'id': '0IeKtwj'
    },
    {
      'catId': '5bd8c46fab67b55ce966fbf9',
      'descr': 'sdfsdf',
      'sum': 261747,
      'type': 'income',
      'id': 'BTpZCXc'
    },
    {
      'catId': '5bd8c46f36fbc0b8e5436ced',
      'descr': 'rwer',
      'sum': 234,
      'type': 'consumption',
      'id': 'xCanY5K'
    },
    {
      'catId': '5bd8c46f51a4128eb96df054',
      'descr': 'werwer',
      'sum': 400,
      'type': 'income',
      'id': 'U~xEwwd'
    },
    {
      'catId': '5bd8c46f0c93161cbcace56f',
      'descr': 'werwerwerwerwer',
      'sum': 5000,
      'type': 'consumption',
      'id': 'Cg5swSR'
    },
    {
      'catId': '5bd8c46f51a4128eb96df054',
      'descr': 'hjgh',
      'sum': 86,
      'type': 'income',
      'id': 'QkyxGV_'
    }
  ];
  categories = [
    {
      'id': '5bd8c46fc1b416ae341c922f',
      'name': 'Продукты питания',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46f561eca76ba7d1468',
      'name': 'Авто и транспорт',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46f36fbc0b8e5436ced',
      'name': 'Одежда и обувь',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46f0c93161cbcace56f',
      'name': 'Квартира',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46f402ca41286931379',
      'name': 'Кафе и рестораны',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46f309cc243de3ee2dd',
      'name': 'Развлечения',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46fd03c03ebf2b728a5',
      'name': 'Путешествия',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46fa6905822d10ce177',
      'name': 'Здоровье',
      'type': 'consumption'
    },
    {
      'id': '5bd8c46fab67b55ce966fbf9',
      'name': 'Основная работа',
      'type': 'income'
    },
    {
      'id': '5bd8c46fc5774aa7d174202c',
      'name': 'Дополнительный источник',
      'type': 'income'
    },
    {
      'id': '5bd8c46f51a4128eb96df054',
      'name': 'Возврат долга',
      'type': 'income'
    },
    {
      'id': '5bd8c46f1257fe7d1c2a3adb',
      'name': 'Проценты',
      'type': 'income'
    }
  ];

  dispOperation(operation) {
    console.log(operation.id);
    console.log(operation.catId);
    console.log(operation.type);
    console.log(operation.sum);
    console.log(operation.descr);
  };

  constructor() { }
  
  ngOnInit() {
    
    // 1) Методы по работе с массивами ES6. (map, filter, find, entry). 
    // EXAMPLE: у вас есть операции. Создайте 2 массива, в одном будут операции дохода, 
    // в другом расхода. Потом, к доходным операциям в описание добавить “ДОХОД - ”
    
    var incOperations = this.operations
    .filter((elem) => { return elem.type === 'income';  })
    .map((elem) => {
      var elem1 = {... elem};
      elem1.descr = 'ДОХОД - ' + elem.descr;
      return elem1;
    });
    
    var consOperations = this.operations
    .filter((elem) => { return elem.type === 'consumption'; });

    // console.log(incOperations);
    // console.log(consOperations);

    

  }

}

export interface Operation {
  'id': string,
  'catId': string,
  'type': string,
  'sum': number,
  'descr': string,
  }