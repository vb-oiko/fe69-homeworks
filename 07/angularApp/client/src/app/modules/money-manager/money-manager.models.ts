export interface IOperation {
  id: string;
  catId: string;
  descr: string;
  sum: number;
  type: OperationType;
}

export interface IOperationCategory {
  id: string;
  name: string;
  type: OperationType;
}

export enum OperationType {
  income = 'income',
  consumption = 'consumption'
}
