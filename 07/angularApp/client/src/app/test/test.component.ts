import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent {

  constructor() { }

  // private x = 10;
  // x: number = 10;
  // x: string = '10';
  // x: boolean = true;
  // arr: number[] = [3, 5, 7];

  // human: IHuman = {
  //   name: 'Bob',
  //   age: 40,
  //   email: 'bob@gmail.com'
  // };

  // humans: IHuman[] = [
  //   {
  //     name: 'Bob',
  //     age: 40,
  //     email: 'bob@gmail.com'
  //   },
  //   {
  //     name: 'Bob',
  //     age: 40,
  //     email: 'bob@gmail.com'
  //   }
  // ];

  human: IHuman = {
    name: 'Bob',
    age: 40,
    email: 'bob@gmail.com',
    gender: Gander.male
  };

  human2: IHuman = {
    name: 'Kate',
    age: 40,
    email: 'bob1@gmail.com',
    gender: Gander.female
  };

  human3: IHuman = {
    name: 'Golubok',
    age: 40,
    email: 'golubok_cem_cem@gmail.com',
    gender: Gander.male,
    location: {
      city: 'Amsterdam',
      country: 'Netherlands'
    }
  };

}

interface IHuman {
  name: string;
  age: number;
  email: string;
  surname?: string;
  gender: Gander;
  location?: ILocation;
}

enum Gander {
  male = 'male',
  female = 'female',
}

interface ILocation {
  city: string;
  country: string;
  index?: string;
}
