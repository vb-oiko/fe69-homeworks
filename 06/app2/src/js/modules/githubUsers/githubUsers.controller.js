export var GithubUsersController = function (model, view) {
    return {
        init: function () {
            model.getUsers(function (users) {
                view.render('allUsers', users);
            });
            view.listen('getUser', function (login) {
                model.getUser(login, function (user) {
                    view.render('userProfile', user);
                });
            });
            view.listen('searchUser', function (login) {
                model.getUser(login, function (user) {
                    view.render('userProfile', user);
                });
            });
            view.listen('notification', function (msg) {
                view.render('notification', msg);
            });


        }
    }
}