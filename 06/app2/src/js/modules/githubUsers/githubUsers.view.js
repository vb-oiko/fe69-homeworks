export var GithubUsersView = function (moduleName) {
    var S = {
        usersList: $('[data-module=' + moduleName + '] .app__users-list'),
        userProfile: $('[data-module=' + moduleName + '] .app__user'),
        userSearchInput: $('[data-module=' + moduleName + '] .user-search__input'),
        userSearchBtn: $('[data-module=' + moduleName + '] .user-search__btn'),
        notification: $('[data-module=' + moduleName + '] .app__notification'),
    }
    var templates = {
        userItem: '<div class="app__users-item"><img src="{{avatar_url}}" width="100">{{login}}<button class="app__users-btn" data-login="{{login}}">Get Data</button></div>',
        userProfile: `
            <img src="{{avatar_url}}">
            <h1>{{name}}</h1>
        `
    }
    return {
        render: function (renderType, data) {
            var renderers = {
                'allUsers': function () {
                    for (var i = 0; i < data.length; i++) {
                        S.usersList.append(
                            templates.userItem
                            .replace(/{{login}}/g, data[i].login)
                            .replace('{{avatar_url}}', data[i].avatar_url)
                        )
                    }
                },
                'userProfile': function () {
                    S.userProfile.html(templates.userProfile.replace('{{name}}', data.name).replace('{{avatar_url}}', data.avatar_url));
                },
                'notification': function () {
                    S.notification.html(data);
                    S.notification.show();
                    
                }
            }
            renderers[renderType]();
        },
        listen: function (eventName, cb) {
            switch (eventName) {
                case 'getUser':
                    S.usersList.on('click', '.app__users-btn', function () {
                        var login = $(this).attr('data-login');
                        cb(login)
                    })
                    break;
                case 'searchUser':
                    S.userSearchBtn.click(function () {
                        let searchLogin = S.userSearchInput.val();
                        if (searchLogin) {
                            cb(searchLogin);
                        }
                    })
                    break;
                case 'notification':
                    $(document).on('notification', function (event, message) {
                        cb(message);
                    });
                    break;
            }
        }
    }
}