export var GithubUsersModel = (function () {
    return {
        getUsers: function (successCb) {
            $.get('https://api.github.com/users')
                .done(function (res) {
                    successCb(res);
                })
                .fail(function (f) {
                    $(document).trigger('notification','Server error!')
                })
            },
            getUser: function (login, successCb) {
                $.get('https://api.github.com/users/' + login)
                .done(function (res) {
                    successCb(res);
                    $(document).trigger('notification','User found successfully!')
                })
                .fail(function (f) {
                    $(document).trigger('notification','User not found!')
                })
        }
    }
}())