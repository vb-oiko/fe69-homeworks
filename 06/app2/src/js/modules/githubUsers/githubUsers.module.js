import {
    GithubUsersController
} from './githubUsers.controller';
import {
    GithubUsersModel
} from './githubUsers.model';
import {
    GithubUsersView
} from './githubUsers.view';

export var GithubUsersModule = (function () {
    var moduleName = 'githubUsers';
    var view = GithubUsersView(moduleName);
    var model = GithubUsersModel;
    var controller = GithubUsersController(model, view);
    return {
        init: function () {
            controller.init()
        }
    }
}());