// $('#getData').click(function () {
//     $.ajax({
//         type: 'GET',
//         url: 'https://api.github.com/users',
//         success: function (res) {
//             console.log(res);
//         }
//     })
// })

import {
    GithubUsersModule
} from './modules/githubUsers/githubUsers.module';

var App = (function () {
    return {
        init: function () {
            GithubUsersModule.init()
        }
    }
}());
App.init();

// ES 6
// let
// const CONSTANT1 = 10;
// const CONSTANT1 = {
//     a: 20
// };
// console.log(CONSTANT1);
// CONSTANT1.a = 40;
// console.log(CONSTANT1);

// let man = {
//     name: 'Bob',
//     age: 40
// }

// let man2 = {
//     ...man,
//     email: 'bob@gmail.com'
// }

// console.log(man2);


// let params = [40, 50];
// let f1 = function (a, b) {
//     return a + b;
// }
// console.log(f1(...params));

// let login = 'Bob';
// let template = `
//     <div>
//         <h1>${login}</h1>
//     </div>
// `;


// () => {}



// class Car {
//     constructor(model) {
//         this._model = model;
//     }

//     get model() {
//         return this._model + ')))';
//     }
// }

// let car1 = new Car('ddd');
// console.log(car1.model);


// var Car = function (model) {
//     this.foo = model;
// }
// Car.prototype.engine = function () {  }

// let car1 = new Car('lancer');

// var x = 10;
// var y = 20;
// var z = {
//     x,
//     y
// }
// console.log(z);

// var arr = [3, 5, 6, 9, 18];
// console.log(arr.some((a) => a === 10));