var App = (function () {
    return {
        init: function () {
            TableController.init();
        }
    }
} ());

App.init();