var TableController = (function () {
    return {
            init : function () {
                
                TableModel.get('products', function (products) {
                    TableView.render('products', products);
                });

                TableModel.get('cart', function (cart) {
                    TableView.render('cart', cart);
                });
                
                TableModel.get('total', function (total) {
                    TableView.render('total', total);
                });
                
                TableView.listen('addBtn', function (itemId){
                    TableModel.addToCart(itemId, function (cart, total) {
                        TableView.render('cart', cart);
                        TableView.render('total', total);

                    });
                });
        }
    }
} ());