var NotesView = (function () {

    var tmpl = {
        // note: '<div class="note"><b><span class="noteId">{{id}}</span></b> {{text}} <button class="delBtn" data-id="{{id}}">x</button></div>',
        note: '<div class="note">{{text}} <button class="delBtn" data-id="{{id}}">x</button></div>',
        notesCount: 'Total notes: {{count}}'
    };

    var sel = {
        notes: document.querySelector('.container'),
        addBtn: document.querySelector('.btnAddNote'),
        newNoteTextInp: document.querySelector('.inpNewNote'),
        inpErrMsg: document.querySelector('.err-msg'),
        notesCount: document.querySelector('.notes-count')
    };
    
    var settings = {
        noteLength: 160
    }

    var msg = {
        tooLong: 'Note must not exceed ' + settings.noteLength + ' characters',
        empty: 'Note cannot be empty'
    }

    return {
        
        init: function () {
            sel.newNoteTextInp.addEventListener('keypress', function(event) {
                if (event.keyCode == 13){
                    event.preventDefault();
                    sel.addBtn.click();
                }
            });
        },

        render: function (rendererName, data) {
            var renderers = {
                notes: function () {
                    let html = '';
                    let s = ''
                    for (let i=0;i<data.length;i++) {
                        s = tmpl.note;
                        s = s.replace('{{text}}', data[i].text);
                        s = s.replace(/{{id}}/gi, data[i].id);
                        html += s;
                    };
        
                    sel.notes.innerHTML = html;
                    sel.newNoteTextInp.focus();

                },

                errMsg: function () {
                    
                    if (data === '') {
                        sel.inpErrMsg.classList.remove('err-msg-display');
                        sel.newNoteTextInp.classList.remove('border-red');
                        sel.inpErrMsg.innerHTML = '';
                    } else {
                        sel.inpErrMsg.classList.add('err-msg-display');
                        sel.newNoteTextInp.classList.add('border-red');
                        sel.inpErrMsg.innerHTML = data;

                    };

                },

                notesCount: function () {

                    if (data === 0) {
                        sel.notesCount.innerHTML = '';
                    }
                    else { 
                        s = tmpl.notesCount;
                        s = s.replace('{{count}}', data);
                        sel.notesCount.innerHTML = s;
                    }
                }
            }

            if (!renderers[rendererName]) {
                console.log("Renderer " + rendererName + "doesn't exist");
                return;
            };

            renderers[rendererName]();
        },
        
        validateNoteText: function (newNoteText) {
            
            if (newNoteText.length === 0) {
                NotesView.render('errMsg', msg.empty);
                return false;
            };
            
            if (newNoteText.length > settings.noteLength) {
                NotesView.render('errMsg', msg.tooLong);
                return false;
            };
            
            NotesView.render('errMsg', '');
            return true;
        },

        listen: function (listenerName, callback) {
            var listeners = {
                newNoteBtn: function () {
                    sel.addBtn.addEventListener('click', function (target) {
                        let newNoteText = sel.newNoteTextInp.value;
                        if (NotesView.validateNoteText(newNoteText)) {
                            callback(newNoteText);
                            sel.newNoteTextInp.value = '';
                        }
    
                    });
                },

                deleteNoteBtns: function () {
                    sel.notes.addEventListener('click', function (ev) {
                        let noteId = ev.target.getAttribute('data-id');
                        if (noteId != null) callback (noteId);
                    })
                }

            }

            if (!listeners[listenerName]) {
                console.log("Listener " + listenerName + "doesn't exist");
                return;
            };

            listeners[listenerName]();

        },




    }
} ());