var NotesController = (function () {
    return {
        init: function () {
            console.log('Controller init!!!');
            // let notes;

            NotesView.init();
        
            NotesModel.getNotes(function (notes) {
                NotesView.render('notes', notes);
            });

            NotesModel.getNotesCount (function (count) {
                NotesView.render('notesCount', count);
            });

            NotesView.listen( 'newNoteBtn', function (newTextNote) {
                NotesModel.addNote(newTextNote, function (notes) {
                    NotesView.render('notes', notes);
                });
                NotesModel.getNotesCount (function (count) {
                    NotesView.render('notesCount', count);
                });
            });
            
            NotesView.listen('deleteNoteBtns', function (noteId) {
                NotesModel.deleteNote (noteId, function (notes) {
                    NotesView.render('notes', notes);
                })
                NotesModel.getNotesCount (function (count) {
                    NotesView.render('notesCount', count);
                });
            });

        }

        

    };
} ());