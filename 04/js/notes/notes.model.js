var NotesModel = (function () {

    var NOTES = [
        {
            id: 1,
            text: 'Some note text 1'
        },
        {
            id: 2,
            text: 'Some note text 2'
        },
        {
            id: 3,
            text: 'Some note text 3'
        },
        {
            id: 4,
            text: 'Some note text 4'
        },
    ];

    return {
        getNotes: function (callback) {
            callback(NOTES);
            return NOTES;
        },
        
        addNote: function (newNoteText, callback) {
            let newId = 0;
            
            if (NOTES.length === 0) newId = 0
            else newId = NOTES[NOTES.length-1].id + 1;
            
            var newNote = {id: newId, text: newNoteText};
            NOTES.push(newNote);

            callback(NOTES);
        },
        
        deleteNote: function (noteId, callback) {
            for (i=0;i<NOTES.length;i++) {
                if (NOTES[i].id == noteId) {
                    NOTES.splice(i,1);
                    break;
                };
            }
            callback(NOTES);
        },

        getNotesCount: function (callback) {
            callback (NOTES.length);
        }


    }
} ());