//http://api.jquery.com/on/

var $ = function (selector) {

    var elements = document.querySelectorAll(selector);

    var iterate = function (cbIterate) {
        for (i=0;i<elements.length;i++) {
            cbIterate(elements[i]);
        };
    };

    return {
        on: function (event, cbOn) {
            iterate(function (el) {
                el.addEventListener(event, cbOn)
            })
        }
    }
};

$('.circle').on('click', function () {
    console.log('Element ID: ', this.getAttribute('data-id'));
    
});